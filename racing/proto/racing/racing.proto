syntax = "proto3";
package racing;

option go_package = "/racing";

import "google/protobuf/timestamp.proto";

service Racing {
  // ListRaces will return a collection of all races.
  rpc ListRaces(ListRacesRequest) returns (ListRacesResponse) {}
  // GetRace will return a single race with the provided race_id
  rpc GetRace(GetRaceRequest) returns (GetRaceResponse) {}
}

/* Requests/Responses */

message ListRacesRequest {
  ListRacesRequestFilter filter = 1;
}

// Response to ListRaces call.
message ListRacesResponse {
  repeated Race races = 1;
}

// Filter for listing races.
message ListRacesRequestFilter {
  repeated int64 meeting_ids = 1;
  // visible will display only visible races if true, or all races otherwise
  bool visible = 2;
  // Do order_by as an enum so we can allow fields to be set, but prevent any potential SQL injection from allowing strings
  // I would love to use a string for this but mysql doesn't allow dynamic values in ORDER BY / GROUP BY statements
  // we could do a simple regex filter to prevent characters like ';" etc. but this seems much more secure, albeit a little more work
  enum OrderBy {
    ADVERTISED_START_TIME_ASC = 0;
    ADVERTISED_START_TIME_DESC = 1;
    ID_ASC = 2;
    ID_DESC = 3;
    MEETING_ID_ASC = 4;
    MEETING_ID_DESC = 5;
    NAME_ASC = 6;
    NAME_DESC = 7;
    NUMBER_ASC = 8;
    NUMBER_DESC = 9;
    VISIBLE_ASC = 10;
    VISIBLE_DESC = 11;
  }
  repeated OrderBy order_by = 3;
}

message GetRaceRequest {
  int64 id = 1;
}

message GetRaceResponse {
  Race race = 1;
}

/* Resources */

// A race resource.
message Race {
  // ID represents a unique identifier for the race.
  int64 id = 1;
  // MeetingID represents a unique identifier for the races meeting.
  int64 meeting_id = 2;
  // Name is the official name given to the race.
  string name = 3;
  // Number represents the number of the race.
  int64 number = 4;
  // Visible represents whether or not the race is visible.
  bool visible = 5;
  // AdvertisedStartTime is the time the race is advertised to run.
  google.protobuf.Timestamp advertised_start_time = 6;
  // Status is whether the race is OPEN or CLOSED
  enum Status {
    OPEN = 0;
    CLOSED = 1;
  } 
  Status status = 7;
}

