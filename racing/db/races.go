package db

import (
	"database/sql"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/golang/protobuf/ptypes"
	_ "github.com/mattn/go-sqlite3"

	"git.neds.sh/matty/entain/racing/proto/racing"
)

// RacesRepo provides repository access to races.
type RacesRepo interface {
	// Init will initialise our races repository.
	Init() error

	// List will return a list of races.
	List(filter *racing.ListRacesRequestFilter) ([]*racing.Race, error)

	// Get will return a single race
	Get(id int64) (*racing.Race, error)
}

type racesRepo struct {
	db   *sql.DB
	init sync.Once
}

// NewRacesRepo creates a new races repository.
func NewRacesRepo(db *sql.DB) RacesRepo {
	return &racesRepo{db: db}
}

// Init prepares the race repository dummy data.
func (r *racesRepo) Init() error {
	var err error

	r.init.Do(func() {
		// For test/example purposes, we seed the DB with some dummy races.
		err = r.seed()
	})

	return err
}

// Get will return a single race
func (r *racesRepo) Get(id int64) (*racing.Race, error) {
	var (
		err   error
		query string
		args  []interface{}
	)

	query = getRaceQueries()[racesList]

	query += " WHERE id = ?"
	args = append(args, id)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	races, err := r.scanRaces(rows)
	return races[0], nil
}

func (r *racesRepo) List(filter *racing.ListRacesRequestFilter) ([]*racing.Race, error) {
	var (
		err   error
		query string
		args  []interface{}
	)

	query = getRaceQueries()[racesList]

	query, args = r.applyFilter(query, filter)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	return r.scanRaces(rows)
}

func (r *racesRepo) applyFilter(query string, filter *racing.ListRacesRequestFilter) (string, []interface{}) {
	var (
		clauses       []string
		args          []interface{}
		orderByFields []string
	)

	if filter == nil {
		return query, args
	}

	if len(filter.MeetingIds) > 0 {
		clauses = append(clauses, "meeting_id IN ("+strings.Repeat("?,", len(filter.MeetingIds)-1)+"?)")

		for _, meetingID := range filter.MeetingIds {
			args = append(args, meetingID)
		}
	}

	// add filter for showing visible races
	// show only visible races when visible is set to true, otherwise show all races
	if filter.Visible {
		clauses = append(clauses, "visible = ?")
		args = append(args, filter.Visible)
	}

	if len(clauses) != 0 {
		query += " WHERE " + strings.Join(clauses, " AND ")
	}

	// generate the order_by string from provided enum values
	if len(filter.OrderBy) > 0 {
		for _, e := range filter.OrderBy {
			switch e {
			case racing.ListRacesRequestFilter_ADVERTISED_START_TIME_ASC:
				orderByFields = append(orderByFields, "advertised_start_time")
			case racing.ListRacesRequestFilter_ADVERTISED_START_TIME_DESC:
				orderByFields = append(orderByFields, "advertised_start_time DESC")
			case racing.ListRacesRequestFilter_ID_ASC:
				orderByFields = append(orderByFields, "id")
			case racing.ListRacesRequestFilter_ID_DESC:
				orderByFields = append(orderByFields, "id DESC")
			case racing.ListRacesRequestFilter_MEETING_ID_ASC:
				orderByFields = append(orderByFields, "meeting_id")
			case racing.ListRacesRequestFilter_MEETING_ID_DESC:
				orderByFields = append(orderByFields, "meeting_id DESC")
			case racing.ListRacesRequestFilter_NAME_ASC:
				orderByFields = append(orderByFields, "name")
			case racing.ListRacesRequestFilter_NAME_DESC:
				orderByFields = append(orderByFields, "name DESC")
			case racing.ListRacesRequestFilter_NUMBER_ASC:
				orderByFields = append(orderByFields, "number")
			case racing.ListRacesRequestFilter_NUMBER_DESC:
				orderByFields = append(orderByFields, "number DESC")
			case racing.ListRacesRequestFilter_VISIBLE_ASC:
				orderByFields = append(orderByFields, "visible")
			case racing.ListRacesRequestFilter_VISIBLE_DESC:
				orderByFields = append(orderByFields, "visible DESC")
			}
		}
		if len(orderByFields) > 0 {
			query += fmt.Sprintf(" ORDER BY %s", strings.Join(orderByFields, ", "))
		}
	} else {
		// if no order by query was set, default behavior should be advertised_start_time
		query += fmt.Sprintf(" ORDER BY advertised_start_time")
	}

	return query, args
}

func (m *racesRepo) scanRaces(
	rows *sql.Rows,
) ([]*racing.Race, error) {
	var races []*racing.Race

	for rows.Next() {
		var race racing.Race
		var advertisedStart time.Time

		if err := rows.Scan(&race.Id, &race.MeetingId, &race.Name, &race.Number, &race.Visible, &advertisedStart); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}

			return nil, err
		}

		ts, err := ptypes.TimestampProto(advertisedStart)
		if err != nil {
			return nil, err
		}

		race.AdvertisedStartTime = ts

		// Check the status of races and set that they are closed if the advertised_start_time is before now
		// We only need to set closed because races will be open by default
		if advertisedStart.Before(time.Now()) {
			race.Status = racing.Race_CLOSED
		}

		races = append(races, &race)
	}

	return races, nil
}
