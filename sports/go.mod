module git.neds.sh/matty/entain/sports

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
	google.golang.org/genproto v0.0.0-20210707164411-8c882eb9abba // indirect
	google.golang.org/grpc v1.39.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/protobuf v1.27.1
	syreclabs.com/go/faker v1.2.3
)
