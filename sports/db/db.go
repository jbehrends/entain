package db

import (
	"time"

	"syreclabs.com/go/faker"
)

func (r *sportsRepo) seed() error {
	statement, err := r.db.Prepare(`CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY, name TEXT, visible INTEGER, advertised_start_time DATETIME, status TEXT)`)
	if err == nil {
		_, err = statement.Exec()
	}

	for i := 1; i <= 100; i++ {
		statement, err = r.db.Prepare(`INSERT OR IGNORE INTO events(id, name, visible, advertised_start_time, status) VALUES (?,?,?,?,?)`)
		if err == nil {
			advertised_start_time := faker.Time().Between(time.Now().AddDate(0, 0, -1), time.Now().AddDate(0, 0, 2))
			status := "OPEN"
			if advertised_start_time.Before(time.Now()) {
				status = "CLOSED"
			}
			_, err = statement.Exec(
				i,
				faker.Team().Name(),
				faker.Number().Between(0, 1),
				advertised_start_time.Format(time.RFC3339),
				status,
			)
		}
	}

	return err
}
