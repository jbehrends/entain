package db

import (
	"database/sql"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/golang/protobuf/ptypes"
	_ "github.com/mattn/go-sqlite3"

	"git.neds.sh/matty/entain/sports/proto/sports"
)

// SportsRepo provides repository access to sports.
type SportsRepo interface {
	// Init will initialise our sports event repository.
	Init() error

	// List will return a list of sports events.
	List(filter *sports.ListEventsRequestFilter) ([]*sports.Event, error)
}

type sportsRepo struct {
	db   *sql.DB
	init sync.Once
}

// NewSportsRepo creates a new sports repository.
func NewSportsRepo(db *sql.DB) SportsRepo {
	return &sportsRepo{db: db}
}

// Init prepares the sports repository dummy data.
func (r *sportsRepo) Init() error {
	var err error

	r.init.Do(func() {
		// For test/example purposes, we seed the DB with some dummy sports.
		err = r.seed()
	})

	return err
}

// List out all the events, filtered by an optional filter
func (r *sportsRepo) List(filter *sports.ListEventsRequestFilter) ([]*sports.Event, error) {
	var (
		err   error
		query string
		args  []interface{}
	)

	query = getEventQueries()[eventsList]

	query, args = r.applyFilter(query, filter)

	rows, err := r.db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	return r.scanEvents(rows)
}

func (r *sportsRepo) applyFilter(query string, filter *sports.ListEventsRequestFilter) (string, []interface{}) {
	var (
		clauses       []string
		args          []interface{}
		orderByFields []string
	)

	if filter == nil {
		return query, args
	}

	// if they provide an id just return the event with that id
	if filter.Id != 0 {
		clauses = append(clauses, "id = ?")
		args = append(args, filter.Id)
	}

	// if they provide a name filter then filter events with a name similar to the filter they provide
	if filter.Name != "" {
		clauses = append(clauses, "name LIKE (?)")
		args = append(args, "%"+filter.Name+"%")
	}

	// add filter for showing visible sports
	// show only visible sports when visible is set to true, otherwise show all sports
	if filter.Visible {
		clauses = append(clauses, "visible = ?")
		args = append(args, filter.Visible)
	}

	if filter.Status != "" {
		clauses = append(clauses, "status = ?")
		args = append(args, filter.Status)
	}

	if len(clauses) != 0 {
		query += " WHERE " + strings.Join(clauses, " AND ")
	}

	// generate the order_by string from provided enum values
	if len(filter.OrderBy) > 0 {
		for _, e := range filter.OrderBy {
			switch e {
			case sports.ListEventsRequestFilter_ADVERTISED_START_TIME_ASC:
				orderByFields = append(orderByFields, "advertised_start_time")
			case sports.ListEventsRequestFilter_ADVERTISED_START_TIME_DESC:
				orderByFields = append(orderByFields, "advertised_start_time DESC")
			case sports.ListEventsRequestFilter_ID_ASC:
				orderByFields = append(orderByFields, "id")
			case sports.ListEventsRequestFilter_ID_DESC:
				orderByFields = append(orderByFields, "id DESC")
			case sports.ListEventsRequestFilter_NAME_ASC:
				orderByFields = append(orderByFields, "name")
			case sports.ListEventsRequestFilter_NAME_DESC:
				orderByFields = append(orderByFields, "name DESC")
			case sports.ListEventsRequestFilter_VISIBLE_ASC:
				orderByFields = append(orderByFields, "visible")
			case sports.ListEventsRequestFilter_VISIBLE_DESC:
				orderByFields = append(orderByFields, "visible DESC")
			case sports.ListEventsRequestFilter_STATUS_ASC:
				orderByFields = append(orderByFields, "status")
			case sports.ListEventsRequestFilter_STATUS_DESC:
				orderByFields = append(orderByFields, "status DESC")
			}
		}
		if len(orderByFields) > 0 {
			query += fmt.Sprintf(" ORDER BY %s", strings.Join(orderByFields, ", "))
		}
	} else {
		// if no order by query was set, default behavior should be advertised_start_time
		query += fmt.Sprintf(" ORDER BY advertised_start_time")
	}

	return query, args
}

// reformat advertised_start_date into a prototype timestamp
func (m *sportsRepo) scanEvents(rows *sql.Rows) ([]*sports.Event, error) {
	var events []*sports.Event

	for rows.Next() {
		var event sports.Event
		var advertisedStart time.Time

		if err := rows.Scan(&event.Id, &event.Name, &event.Visible, &advertisedStart, &event.Status); err != nil {
			if err == sql.ErrNoRows {
				return nil, nil
			}

			return nil, err
		}

		ts, err := ptypes.TimestampProto(advertisedStart)
		if err != nil {
			return nil, err
		}

		event.AdvertisedStartTime = ts

		events = append(events, &event)
	}

	return events, nil
}
