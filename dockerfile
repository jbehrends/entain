FROM golang:latest

RUN apt-get update && apt-get install -y protobuf-compiler

RUN go get github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 google.golang.org/genproto/googleapis/api google.golang.org/grpc/cmd/protoc-gen-go-grpc google.golang.org/protobuf/cmd/protoc-gen-go

WORKDIR /app

RUN go get -u github.com/beego/bee

CMD ["bee", "run", "app", "true"]